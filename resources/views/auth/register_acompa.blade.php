@extends('layouts.auths.app')

@section('content')
<div class="container">
    <div class="col-md-12 content-center">
        <div class="card-plain">
                @error('firstname')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @enderror
                @error('lastname')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @enderror
                @error('email')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @enderror
                @error('password')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @enderror
            <form class="form" method="POST" action="{{ route('register') }}">
                @csrf
                <input type="hidden" name="role" value="100">
                <div class="header">
                    <div class="logo-container">
                        <img src="{{asset('assets\images\logo.png')}}" alt="">
                    </div>
                    <h5>{{ __('Register') }}</h5>
                <span>{{ __('auth/register.register_new_acompa')}}</span>
                </div>
                <div class="content">                                                
                    <div class="input-group">
                    <input name="firstname" required type="text" class="form-control" placeholder="{{__('auth/register.firstname')}}">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-account-circle"></i>
                        </span>
                        
                    </div>
                    <div class="input-group">
                        <input name="lastname" required type="text" class="form-control" placeholder="{{ __('auth/register.lastname')}}">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-account-circle"></i>
                        </span>
                        
                    </div>
                    <div class="input-group">
                        <input required name="email" type="email" class="form-control" placeholder="{{ __('E-Mail Address') }}">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-email"></i>
                        </span>
                    </div>
                    <div class="input-group">
                        <input required name="password" type="password" placeholder="{{ __('Password') }}" class="form-control">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                        </span>
                    </div> 
                    <div class="input-group">
                        <input required name="password_confirmation" type="password" placeholder="{{ __('Confirm Password') }}" class="form-control">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                        </span>
                    </div>                        
                </div>
                <div class="checkbox">
                        <input name="terms" value="true" required id="terms" type="checkbox">
                        <label for="terms">
                                @php
                                    echo __('auth/register.terms_conditions')
                                @endphp
                        </label>
                    </div>
                <div class="footer text-center">
                    <button type="submit" class="btn btn-primary btn-round btn-block  waves-effect waves-light">{{ __('Register') }}</button>
                    <h5><a class="link" href="{{route('login')}}">{{__('auth/register.already_account')}}</a></h5>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
