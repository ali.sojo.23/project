@extends('layouts.auths.app')

@section('content')

<div class="container">
    <div class="col-md-12 content-center">
        <div class="card-plain">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @error('email')
                <div class="alert alert-danger" role="alert">
                    {{ $message }}
                </div>
            @enderror
            <form class="form" method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="header">
                    <div class="logo-container">
                        <img src="{{asset('assets\images\logo.png')}}" alt="">
                    </div>
                    <h5>{{ __('Reset Password') }}</h5>
                <span>{{__('auth/email.message')}}</span>
                </div>
                <div class="content">
                    <div class="input-group">
                        <input name="email" type="text" class="form-control" placeholder="{{ __('E-Mail Address') }}">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-email"></i>
                        </span>
                    </div>
                </div>
                <div class="footer text-center">
                    <button type="submit" class="btn btn-primary btn-round btn-block  waves-effect waves-light">{{ __('Send Password Reset Link') }}</button>
                    <h5><a href="{{__('auth/email.help_url')}}" target="_blank" class="link">{{__('auth/email.need_help')}}</a></h5>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
