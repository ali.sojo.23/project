@extends('layouts.auths.app')

@section('content')
<div class="container">
    <div class="col-md-12 content-center">
        <div class="card-plain">
            @error('email')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @enderror
                @error('password')
                    <div class="alert alert-danger" role="alert">
                        {{ $message }}
                    </div>
                @enderror

            <form class="form" method="POST" action="{{ route('password.update') }}">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="header">
                    <div class="logo-container">
                        <img src="{{asset('assets\images\logo.png')}}" alt="">
                    </div>
                    <h5>{{ __('Reset Password') }}</h5>
                </div>
                <div class="content">
                    <div class="input-group">
                        <input name="email" type="text" class="form-control" value="{{ $email ?? old('email') }}" placeholder="{{ __('E-Mail Address') }}">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-email"></i>
                        </span>

                    </div>
                    <div class="input-group">
                        <input required name="password" type="password" placeholder="{{ __('Password') }}" class="form-control">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                        </span>
                    </div> 
                    <div class="input-group">
                        <input required name="password_confirmation" type="password" placeholder="{{ __('Confirm Password') }}" class="form-control">
                        <span class="input-group-addon">
                            <i class="zmdi zmdi-lock"></i>
                        </span>
                    </div> 
                </div>
                <div class="footer text-center">
                    <button type="submit" class="btn btn-primary btn-round btn-block  waves-effect waves-light">{{ __('Reset Password') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
