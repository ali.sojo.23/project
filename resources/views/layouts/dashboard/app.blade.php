<!doctype html>
<html class="no-js " lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.png" type="image/png">
    @foreach ($styles as $style)
        <link rel="stylesheet" href="{{asset($style)}}">
    @endforeach
    @yield('important')
</head>
<body class="theme-blue">
    {{-- <!-- Page Loader --> --}}
    @include('layouts.dashboard.assets.loader')
    {{-- <!-- Overlay For Sidebars --> --}}
    <div class="overlay"></div>
    {{-- <!-- Top Bar --> --}}
    @include('layouts.dashboard.assets.topbar')
    {{-- <!-- Left Sidebar --> --}}
    @include('layouts.dashboard.assets.leftsidebar')
    {{-- <!-- Right Sidebar --> --}}
    @include('layouts.dashboard.assets.rightsidebar')
    {{-- <!-- Chat-launcher --> --}}
    {{-- @include('layouts.dashboard.assets.chat') --}}
    {{-- <!-- Main Content --> --}}
    
    @yield('content')
    <div id="modal">
        @yield('modal')
    </div>

    {{-- <!-- Jquery Core Js -->  --}}
    @foreach ($scripts as $script)
        <script defer src="{{asset($script)}}"></script>
    @endforeach
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/vertical-menu.js')}}"></script>
</body>
</html>
