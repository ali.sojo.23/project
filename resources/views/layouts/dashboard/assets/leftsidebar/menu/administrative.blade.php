<li class="header">{{ __('dashboard/assets.leftbar.menu.administrative.title')}}</li>
<li class="item-menu-link"><a href="{{route('admin.users.datatable')}}"><i class="zmdi zmdi-account"></i><span>{{ __('dashboard/assets.leftbar.menu.administrative.user')}}</span></a></li>            
<li class="item-menu-link"><a href="{{route('admin.memberships.index')}}"><i class="zmdi zmdi-card-membership"></i><span>{{ __('dashboard/assets.leftbar.menu.administrative.membership')}}</span></a></li>            
<li class="parent-item-menu-link"><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-settings"></i><span>{{ __('dashboard/assets.leftbar.menu.administrative.config.title')}}</span> </a>
    <ul class="ml-menu">
        <li class="item-menu-link"><a href="{{route('admin.settings.rate')}}">{{ __('dashboard/assets.leftbar.menu.administrative.config.exchange_rate')}}</a></li>
    </ul>
</li>
