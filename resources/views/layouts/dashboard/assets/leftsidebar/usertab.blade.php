<div class="tab-pane stretchLeft" id="user">
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info m-b-20 p-b-15">
                    <div class="image"><a href="{{route('profile')}}"><img src="{{Auth::user()->info->img_profile ?? '/assets/images/profile_av.jpg'}} " alt="User"></a></div>
                    <div class="detail">
                        <h4>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h4>
                            <small>
                            @if (Auth::user()->role < 100)
                                {{ __('dashboard/assets.leftbar.profile_card.admin')}}
                            @elseif (Auth::user()->role == 100)
                                {{ __('dashboard/assets.leftbar.profile_card.nurse')}}
                            @else
                                {{ __('dashboard/assets.leftbar.profile_card.user')}}
                            @endif</small>                        
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <a title="facebook" href="#"><i class="zmdi zmdi-facebook"></i></a>
                            <a title="twitter" href="#"><i class="zmdi zmdi-twitter"></i></a>
                            <a title="instagram" href="#"><i class="zmdi zmdi-instagram"></i></a>
                        </div>
                        {{-- <div class="col-4 p-r-0">
                            <h5 class="m-b-5">18</h5>
                            <small>Exp</small>
                        </div>
                        <div class="col-4">
                            <h5 class="m-b-5">125</h5>
                            <small>Awards</small>
                        </div>
                        <div class="col-4 p-l-0">
                            <h5 class="m-b-5">148</h5>
                            <small>Clients</small>
                        </div>                                 --}}
                    </div>
                </div>
            </li>
            @if (Auth::user()->info)
            <li>
                <small class="text-muted">{{ __('dashboard/assets.leftbar.profile_card.location')}}: </small>
                <p>{{ Auth::user()->info->address_1 ?? '' }}, 
                    {{ Auth::user()->info->address_2 ??''}}, 
                    {{ Auth::user()->info->estado->name ??''}}, 
                    {{ Auth::user()->info->pais->name??''}}
                </p>
                <hr>
                <small class="text-muted">{{ __('dashboard/assets.leftbar.profile_card.email')}}:</small>
                <p>{{Auth::user()->email}}</p>
                <hr>
                <small class="text-muted">{{ __('dashboard/assets.leftbar.profile_card.phone')}}: </small>
                <p>{{ Auth::user()->info->phone?? ''}}</p>
                <hr>
                <small class="text-muted">{{ __('dashboard/assets.leftbar.profile_card.website')}}: </small>
                <p>http://dr.charlotte.com/ </p>
                <hr>
                {{-- <ul class="list-unstyled">
                    <li>
                        <div>Colorectal Surgery</div>
                        <div class="progress m-b-20">
                            <div class="progress-bar l-blue " role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="width: 89%"> <span class="sr-only">62% Complete</span> </div>
                        </div>
                    </li>
                    <li>
                        <div>Endocrinology</div>
                        <div class="progress m-b-20">
                            <div class="progress-bar l-green " role="progressbar" aria-valuenow="56" aria-valuemin="0" aria-valuemax="100" style="width: 56%"> <span class="sr-only">87% Complete</span> </div>
                        </div>
                    </li>
                    <li>
                        <div>Dermatology</div>
                        <div class="progress m-b-20">
                            <div class="progress-bar l-amber" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%"> <span class="sr-only">32% Complete</span> </div>
                        </div>
                    </li>
                    <li>
                        <div>Neurophysiology</div>
                        <div class="progress m-b-20">
                            <div class="progress-bar l-blush" role="progressbar" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100" style="width: 43%"> <span class="sr-only">56% Complete</span> </div>
                        </div>
                    </li>
                </ul>                         --}}
            </li>                    
            @endif
        </ul>
    </div>
</div>