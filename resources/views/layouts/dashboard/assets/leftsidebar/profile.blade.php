<li>
    <div class="user-info">
        <div class="image"><a href="{{route('profile')}}"><img src="{{Auth::user()->info->img_profile ?? '/assets/images/profile_av.jpg'}}" alt="User"></a></div>
        <div class="detail">
        <h4>{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</h4>
            <small>
            @if (Auth::user()->role < 100)
                {{ __('dashboard/assets.leftbar.profile_card.admin')}}
            @elseif (Auth::user()->role == 100)
                {{ __('dashboard/assets.leftbar.profile_card.nurse')}}
            @else
                {{ __('dashboard/assets.leftbar.profile_card.user')}}
            @endif</small>                        
        </div>
    </div>
</li>