<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="" src="{{asset('assets\images\logo.png')}}" width="100" alt="acompaño.com"></div>
        <p>{{ __('dashboard/assets.loader.wait')}}</p>        
    </div>
</div>