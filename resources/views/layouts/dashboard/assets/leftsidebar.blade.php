<aside id="leftsidebar" class="sidebar">
    <ul class="nav nav-tabs">
        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#dashboard"><i class="zmdi zmdi-home m-r-5"></i>Home</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#user">@if (Auth::user()->role < 100)
            {{ __('dashboard/assets.leftbar.profile_card.admin')}}
        @elseif (Auth::user()->role == 100)
            {{ __('dashboard/assets.leftbar.profile_card.nurse')}}
        @else
            {{ __('dashboard/assets.leftbar.profile_card.user')}}
        @endif</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
                <ul class="list">
                    @include('layouts.dashboard.assets.leftsidebar.profile')
                    @if (Auth::user()->role < 100)
                        @include('layouts.dashboard.assets.leftsidebar.menu.administrative')
                    @endif
                </ul>
            </div>
        </div>
        @include('layouts.dashboard.assets.leftsidebar.usertab')
    </div>    
</aside>