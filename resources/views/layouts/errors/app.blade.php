<!doctype html>
<html class="no-js " lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') | {{ config('app.name', 'Laravel') }}</title>
    <!-- Favicon-->
    <link rel="icon" href="/favicon.png" type="image/png">
    <!-- Custom Css -->
    <link rel="stylesheet" href="{{asset('assets\plugins\bootstrap\css\bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets\css\main.css')}}">
    <link rel="stylesheet" href="{{asset('assets\css\authentication.css')}}">
    <link rel="stylesheet" href="{{asset('assets\css\color_skins.css')}}">
    <style>
        .authentication .card-plain .logo-container{
            width: 150px;
        }
    </style>
</head>
<body class="theme-cyan authentication sidebar-collapse">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
        <div class="container">        
            <div class="navbar-translate n_logo">
                <a class="navbar-brand" href="javascript:void(0);" title="" target="_blank"></a>
                <button class="navbar-toggler" type="button">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" target="_blank" href="https://xn--acompao-9za.com">Home</a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" title="Follow us on Twitter" href="javascript:void(0);" target="_blank">
                            <i class="zmdi zmdi-twitter"></i>
                            <p class="d-lg-none d-xl-none">Twitter</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" title="Like us on Facebook" href="javascript:void(0);" target="_blank">
                            <i class="zmdi zmdi-facebook"></i>
                            <p class="d-lg-none d-xl-none">Facebook</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" title="Follow us on Instagram" href="javascript:void(0);" target="_blank">                        
                            <i class="zmdi zmdi-instagram"></i>
                            <p class="d-lg-none d-xl-none">Instagram</p>
                        </a>
                    </li>  --}}
                    
                    
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="page-header">
        <div class="page-header-image" style="background-image:url({{asset('assets/images/login.jpg')}})"></div>
        <div class="container">
            <div class="col-md-12 content-center">
                <div class="card-plain">
                    <form class="form" method="" action="">
                        <div class="header">
                            <div class="logo-container">
                                <img src="/assets/images/logo.png" alt="">
                            </div>
                            <h5>@yield('code')</h5>
                            <span>@yield('message')</span>
                        </div>
                        
                        <div class="footer text-center">
                        <a href="{{ url()->previous() }}" class="btn btn-primary btn-round btn-block  waves-effect waves-light">GO TO BACK</a>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <footer class="footer">
            <div class="container">
                <nav>
                    <ul>
                    <li><a href="{{__('auth/assets.contact_us_url')}}" target="_blank">{{__('auth/assets.contact_us')}}</a></li>
                        <li><a href="{{__('auth/assets.about_url')}}" target="_blank">{{__('auth/assets.about')}}</a></li>
                        <li><a href="{{__('auth/assets.blog_url')}}" target="_blank">{{__('auth/assets.blog')}}</a></li>
                        <li><a href="{{__('auth/assets.lang1_url')}}">{{__('auth/assets.lang1')}}</a></li>
                        <li><a href="{{__('auth/assets.lang2_url')}}">{{__('auth/assets.lang2')}}</a></li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script> ACOMPAÑO,
                    <span>Desarrollado por <a href="https://akweb-solutions.com" target="_blank">GRUPO AKWEB C.A.</a> Socio de <a href="https://incgoin.com" target="_blank">INC GOIN</a></span>
                </div>
            </div>
        </footer>
    </div>
    <script src="{{asset('assets\bundles\libscripts.bundle.js')}}"></script>
    <script src="{{asset('assets\bundles\vendorscripts.bundle.js')}}"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>