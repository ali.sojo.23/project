@extends('layouts.dashboard.app') 
@section('important')
    <script src="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.js"></script>
    <link
        href="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.css"
        rel="stylesheet"
    />
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            // Input File
            const inputImage = document.querySelector("#subirImagen");
            // Nodo donde estará el editor
            const editor = document.querySelector("#editorImagen");
            // El canvas donde se mostrará la previa
            const miCanvas = document.querySelector("#previewImagen");
            // Contexto del canvas
            const contexto = miCanvas.getContext("2d");
            // Ruta de la imagen seleccionada
            let urlImage = undefined;
    
            // Evento disparado cuando se adjunte una imagen
            inputImage.addEventListener("change", abrirEditor, false);
            /**
             * Método que abre el editor con la imagen seleccionada
             */
            function abrirEditor(e) {
                // Obtiene la imagen
                urlImage = URL.createObjectURL(e.target.files[0]);
                // Borra editor en caso que existiera una imagen previa
                editor.innerHTML = "";
                let cropprImg = document.createElement("img");
                cropprImg.setAttribute("id", "croppr");
                editor.appendChild(cropprImg);
                // Limpia la previa en caso que existiera algún elemento previo
                contexto.clearRect(0, 0, miCanvas.width, miCanvas.height);
                // Envia la imagen al editor para su recorte
                document.querySelector("#croppr").setAttribute("src", urlImage);
                // Crea el editor
                new Croppr("#croppr", {
                    aspectRatio: 1,
                    startSize: [70, 70],
                    onCropEnd: recortarImagen
                });
            }
            function recortarImagen(data) {
                // Variables
                const inicioX = data.x;
                const inicioY = data.y;
                const nuevoAncho = data.width;
                const nuevaAltura = data.height;
                const zoom = 1;
                let imagenEn64 = "";
                // La imprimo
                miCanvas.width = nuevoAncho;
                miCanvas.height = nuevaAltura;
                // La declaro
                let miNuevaImagenTemp = new Image();
                // Cuando la imagen se carge se procederá al recorte
                miNuevaImagenTemp.onload = function() {
                    // Se recorta
                    contexto.drawImage(
                        miNuevaImagenTemp,
                        inicioX,
                        inicioY,
                        nuevoAncho * zoom,
                        nuevaAltura * zoom,
                        0,
                        0,
                        nuevoAncho,
                        nuevaAltura
                    );
                    // Se transforma a base64
                    imagenEn64 = miCanvas.toDataURL("image/jpeg");
                    // Mostramos el código generado
                    document.querySelector("#codeImagen").value = imagenEn64;
                };
    
                // Proporciona la imagen cruda, sin editarla por ahora
                miNuevaImagenTemp.src = urlImage;
            }
        });
    </script>
    <style>
        #previewImagen {
            max-width: 100%;
        }
    </style>
@endsection
@php 

    $lang = json_encode(__('dashboard/profile')); 
@endphp
@section('content')
<section id="app" class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-5 col-sm-12">
                <h2>
                    @if ($user->role < 100)
                    {{ __("dashboard/profile.profile_title_admin") }}
                    @elseif ($user->role == 100)
                    {{ __("dashboard/profile.profile_title_cuidador") }}
                    @else
                    {{ __("dashboard/profile.profile_title") }}
                    @endif
                    <small>{{
                        __("dashboard/profile.profile_subtitle")
                    }}</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-7 col-sm-12">
                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item">
                        <a href="/"
                            ><i class="zmdi zmdi-home"></i> Home</a
                        >
                    </li>
                    <li class="breadcrumb-item active">@if ($user->role < 100)
                        {{ __("dashboard/profile.profile_title_admin") }}
                        @elseif ($user->role == 100)
                        {{ __("dashboard/profile.profile_title_cuidador") }}
                        @else
                        {{ __("dashboard/profile.profile_title") }}
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-4 col-md-12">
                <dashboard-profile-profile-card
                    :users="{{ $user }}"
                    :trans="{{ $lang }}"
                ></dashboard-profile-profile-card>
                {{-- <div class="card">
                    <div class="body">
                        <div class="workingtime">
                            <h6>Working Time</h6>
                            <small class="text-muted">Tuesday</small>
                            <p>06:00 AM - 07:00 AM</p>
                            <hr />
                            <small class="text-muted">Thursday</small>
                            <p>06:00 AM - 07:00 AM</p>
                            <hr />
                        </div>
                        <div class="reviews">
                            <h6>Reviews</h6>
                            <small class="text-muted">Staff</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                            </p>
                            <hr />
                            <small class="text-muted">Punctuality</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                            </p>
                            <hr />
                            <small class="text-muted">Helpfulness</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                            </p>
                            <hr />
                            <small class="text-muted">Knowledge</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                            </p>
                            <hr />
                            <small class="text-muted">Cost</small>
                            <p>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                                <i class="zmdi zmdi-star-outline"></i>
                            </p>
                            <hr />
                        </div>
                    </div>
                </div>
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a
                                class="nav-link active"
                                data-toggle="tab"
                                href="#Followers"
                                >Followers</a
                            >
                        </li>
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                data-toggle="tab"
                                href="#friends"
                                >Friends</a
                            >
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane body active" id="Followers">
                            <ul class="right_chat list-unstyled">
                                <li class="online">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar4.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Chris Fox</span
                                                >
                                                <span class="message"
                                                    >Designer, Blogger</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="online">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar5.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Joge Lucky</span
                                                >
                                                <span class="message"
                                                    >Java Developer</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="offline">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar2.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Isabella</span
                                                >
                                                <span class="message"
                                                    >CEO, Thememakker</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="offline">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar1.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Folisise Chosielie</span
                                                >
                                                <span class="message"
                                                    >Art director, Movie
                                                    Cut</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="online">
                                    <a href="javascript:void(0);">
                                        <div class="media">
                                            <img
                                                class="media-object "
                                                src="..\assets\images\xs\avatar3.jpg"
                                                alt=""
                                            />
                                            <div class="media-body">
                                                <span class="name"
                                                    >Alexander</span
                                                >
                                                <span class="message"
                                                    >Writter, Mag Editor</span
                                                >
                                                <span
                                                    class="badge badge-outline status"
                                                ></span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane body" id="friends">
                            <ul class="new_friend_list list-unstyled row">
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar1.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Jackson</h6>
                                        <small class="join_date">Today</small>
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar2.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Aubrey</h6>
                                        <small class="join_date"
                                            >Yesterday</small
                                        >
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar3.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Oliver</h6>
                                        <small class="join_date">08 Nov</small>
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar4.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Isabella</h6>
                                        <small class="join_date">12 Dec</small>
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar1.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Jacob</h6>
                                        <small class="join_date">12 Dec</small>
                                    </a>
                                </li>
                                <li class="col-lg-4 col-md-2 col-sm-6 col-4">
                                    <a href="">
                                        <img
                                            src="..\assets\images\sm\avatar5.jpg"
                                            class="img-thumbnail"
                                            alt="User Image"
                                        />
                                        <h6 class="users_name">Matthew</h6>
                                        <small class="join_date">17 Dec</small>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> --}}
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a
                                class="nav-link active"
                                data-toggle="tab"
                                href="#about"
                        >{{ __('dashboard/profile.print_info.about')}} {{$user->firstname}}</a
                            >
                        </li>
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                data-toggle="tab"
                                href="#Account"
                                >{{ __('dashboard/profile.print_info.account')}}</a
                            >
                        </li>
                        <li class="nav-item">
                            <a
                                class="nav-link"
                                data-toggle="tab"
                                href="#verifyAccount"
                                >{{ __('dashboard/profile.print_info.verify')}}</a
                            >
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane body active" id="about">
                            <p>
                              {!!  $user->info->description ?? ''  !!} 
                            </p>
                            @if ($user->info)
                            <h6>{{ __('dashboard/profile.print_info.general_info.title')}}</h6>
                            <hr />
                            <ul class="list-unstyled">
                                <li>
                                    <p><strong>{{ __('dashboard/profile.print_info.general_info.gender')}}</strong> 
                                        @if($user->info->gender == 0) 
                                        {{ __('dashboard/profile.edit_info.gender.male')}}
                                        @elseif($user->info->gender == 1) 
                                        {{ __('dashboard/profile.edit_info.gender.female')}}
                                        @elseif($user->info->gender == 2) 
                                        {{ __('dashboard/profile.edit_info.gender.no_binary')}}
                                        @endif
                                    </p>
                                </li>
                                <li>
                                    <p><strong>{{ __('dashboard/profile.print_info.general_info.birthdate')}}</strong> 
                                        {{ $user->info->birthdate }}
                                    </p>
                                </li>
                            </ul>
                            @endif
                            {{-- <h6>Specialties</h6>
                            <hr />
                            <ul class="list-unstyled specialties">
                                <li>Breast Surgery</li>
                                <li>Colorectal Surgery</li>
                                <li>Endocrinology</li>
                                <li>Cardiology</li>
                                <li>Cosmetic Dermatology</li>
                                <li>Mole checks and monitoring</li>
                                <li>Clinical Neurophysiology</li>
                            </ul> --}}
                        </div>
                        <div class="tab-pane body" id="Account">
                            <dashboard-profile-edit-account-reset-pass
                                :users="{{ $user }}"
                                :trans="{{ $lang }}"
                                :csrf="'{{ Session::token() }}'"
                            ></dashboard-profile-edit-account-reset-pass>
                            <hr />
                            <dashboard-profile-edit-account-info
                                :users="{{ $user }}"
                                :trans="{{ $lang }}"
                            ></dashboard-profile-edit-account-info>
                        </div>
                        <div id="verifyAccount" class="tab-pane body">
                            <dashboard-profile-edit-verification-account-info
                                :auth="{{Auth::user()}}"
                                :user="{{$user}}"
                                :tkn="'{{Session::token()}}'"
                                :lang="{{$lang}}"
                            ></dashboard-profile-edit-verification-account-info>
                        </div>
                    </div>
                </div>
                {{-- <div class="card">
                    <div class="header">
                        <h2><strong>Recent</strong> Activity</h2>
                        <ul class="header-dropdown">
                            <li class="remove">
                                <a role="button" class="boxs-close"
                                    ><i class="zmdi zmdi-close"></i
                                ></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body user_activity">
                        <div class="streamline b-accent">
                            <div class="sl-item">
                                <img
                                    class="user rounded-circle"
                                    src="..\assets\images\xs\avatar4.jpg"
                                    alt=""
                                />
                                <div class="sl-content">
                                    <h5 class="m-b-0">Admin Birthday</h5>
                                    <small
                                        >Jan 21
                                        <a
                                            href="javascript:void(0);"
                                            class="text-info"
                                            >Sophia</a
                                        >.</small
                                    >
                                </div>
                            </div>
                            <div class="sl-item">
                                <img
                                    class="user rounded-circle"
                                    src="..\assets\images\xs\avatar5.jpg"
                                    alt=""
                                />
                                <div class="sl-content">
                                    <h5 class="m-b-0">Add New Contact</h5>
                                    <small
                                        >30min ago
                                        <a href="javascript:void(0);"
                                            >Alexander</a
                                        >.</small
                                    >
                                    <small
                                        ><strong>P:</strong>
                                        +264-625-2323</small
                                    >
                                    <small
                                        ><strong>E:</strong>
                                        maryamamiri@gmail.com</small
                                    >
                                </div>
                            </div>
                            <div class="sl-item">
                                <img
                                    class="user rounded-circle"
                                    src="..\assets\images\xs\avatar6.jpg"
                                    alt=""
                                />
                                <div class="sl-content">
                                    <h5 class="m-b-0">General Surgery</h5>
                                    <small
                                        >Today
                                        <a href="javascript:void(0);">Grayson</a
                                        >.</small
                                    >
                                    <small
                                        >The standard chunk of Lorem Ipsum used
                                        since the 1500s is reproduced</small
                                    >
                                </div>
                            </div>
                            <div class="sl-item">
                                <img
                                    class="user rounded-circle"
                                    src="..\assets\images\xs\avatar7.jpg"
                                    alt=""
                                />
                                <div class="sl-content">
                                    <h5 class="m-b-0">General Surgery</h5>
                                    <small
                                        >45min ago
                                        <a
                                            href="javascript:void(0);"
                                            class="text-info"
                                            >Fidel Tonn</a
                                        >.</small
                                    >
                                    <small
                                        ><strong>P:</strong>
                                        +264-625-2323</small
                                    >
                                    <small
                                        >The standard chunk of Lorem Ipsum used
                                        since the 1500s is reproduced used since
                                        the 1500s is reproduced</small
                                    >
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</section>

@endsection
@section('modal')
    <dashboard-profile-modal-update-img-profile
        :users="{{$user->id}}"
        :trans="{{ $lang }}"
    ></dashboard-profile-modal-update-img-profile>
@endsection