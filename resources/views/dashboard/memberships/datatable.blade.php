@extends('layouts.dashboard.app') 
@section('important')
    <script src="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.js"></script>
    <link
        href="https://cdn.jsdelivr.net/gh/jamesssooi/Croppr.js@2.3.0/dist/croppr.min.css"
        rel="stylesheet"
    />
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            // Input File
            const inputImage = document.querySelector("#subirImagen");
            // Nodo donde estará el editor
            const editor = document.querySelector("#editorImagen");
            // El canvas donde se mostrará la previa
            const miCanvas = document.querySelector("#previewImagen");
            // Contexto del canvas
            const contexto = miCanvas.getContext("2d");
            // Ruta de la imagen seleccionada
            let urlImage = undefined;
    
            // Evento disparado cuando se adjunte una imagen
            inputImage.addEventListener("change", abrirEditor, false);
            /**
             * Método que abre el editor con la imagen seleccionada
             */
            function abrirEditor(e) {
                // Obtiene la imagen
                urlImage = URL.createObjectURL(e.target.files[0]);
                // Borra editor en caso que existiera una imagen previa
                editor.innerHTML = "";
                let cropprImg = document.createElement("img");
                cropprImg.setAttribute("id", "croppr");
                editor.appendChild(cropprImg);
                // Limpia la previa en caso que existiera algún elemento previo
                contexto.clearRect(0, 0, miCanvas.width, miCanvas.height);
                // Envia la imagen al editor para su recorte
                document.querySelector("#croppr").setAttribute("src", urlImage);
                // Crea el editor
                new Croppr("#croppr", {
                    aspectRatio: 1,
                    startSize: [70, 70],
                    onCropEnd: recortarImagen
                });
            }
            function recortarImagen(data) {
                // Variables
                const inicioX = data.x;
                const inicioY = data.y;
                const nuevoAncho = data.width;
                const nuevaAltura = data.height;
                const zoom = 1;
                let imagenEn64 = "";
                // La imprimo
                miCanvas.width = nuevoAncho;
                miCanvas.height = nuevaAltura;
                // La declaro
                let miNuevaImagenTemp = new Image();
                // Cuando la imagen se carge se procederá al recorte
                miNuevaImagenTemp.onload = function() {
                    // Se recorta
                    contexto.drawImage(
                        miNuevaImagenTemp,
                        inicioX,
                        inicioY,
                        nuevoAncho * zoom,
                        nuevaAltura * zoom,
                        0,
                        0,
                        nuevoAncho,
                        nuevaAltura
                    );
                    // Se transforma a base64
                    imagenEn64 = miCanvas.toDataURL("image/jpeg");
                    // Mostramos el código generado
                    document.querySelector("#codeImagen").value = imagenEn64;
                };
    
                // Proporciona la imagen cruda, sin editarla por ahora
                miNuevaImagenTemp.src = urlImage;
            }
        });
    </script>
    <style>
        #previewImagen {
            max-width: 100%;
        }
    </style>
@endsection
@php 

    $lang = json_encode(__('dashboard/memberships')); 
@endphp
@section('content')
<section id="app" class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12">
                <h2>{{ __('dashboard/memberships.title') }}
                </h2>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12">
                <button class="btn btn-white btn-icon btn-round d-none d-md-inline-block float-right m-l-10" data-toggle="modal"
                data-target="#largeModal" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item">
                        <a href="/"
                            ><i class="zmdi zmdi-home"></i> {{ __("dashboard/memberships.slug.home") }}</a
                        >
                    </li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">{{ __("dashboard/memberships.slug.administrative") }}</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="javascript:void(0);">{{ __("dashboard/memberships.slug.memberships") }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card patients-list">
                    <div class="header">
                        <h2>{!!__('dashboard/memberships.subtitle') !!}</h2>
                        
                    </div>
                    <div class="body">
                        
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs padding-0">
                            @foreach ($m as $key => $me)
                                <li  class="nav-item">
                                    <a class="nav-link @if ($loop->first) active @endif" data-toggle="tab" href="#rate{{$key}}">
                                        {{$me->pais->name}}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                            
                        <!-- Tab panes -->
                        <div class="tab-content m-t-10">
                            @foreach ($m as $key => $me)
                                <dashboard-admininistrative-memberships-datatable  
                                    @if ($loop->first)
                                        :active="true"
                                    @else
                                        :active="false"
                                    @endif
                                    :member="{{$me->memberships}}"
                                    :clave="'{{$key}}'"
                                    :lang="{{$lang}}">
                                </dashboard-admininistrative-memberships-datatable>                                
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('modal')
    <dashboard-admininistrative-memberships-modal
        :trans="{{ $lang }}"
    ></dashboard-admininistrative-memberships-modal>
@endsection