@extends('layouts.dashboard.app') 
@php 

    $lang = json_encode(__('dashboard/settings/exchangeRates')); 
@endphp
@section('content')
<section id="app" class="content profile-page">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12">
                <h2>
                    {{ __("dashboard/settings/exchangeRates.title") }}
                    
                </h2>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12">
                
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item">
                        <a href="/"
                            ><i class="zmdi zmdi-home"></i> {{ __("dashboard/settings/exchangeRates.slug.home") }}</a
                        >
                    </li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">{{ __("dashboard/settings/exchangeRates.slug.administrative") }}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="javascript:void(0);">{{ __("dashboard/settings/exchangeRates.slug.settings") }}</a>
                    </li>
                    <li class="breadcrumb-item active">
                        <a href="javascript:void(0);">{{ __("dashboard/settings/exchangeRates.slug.exchange_rates") }}</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active">
                            <a
                                class="nav-link"
                                data-toggle="tab"
                                href="#CreateExchangeRate"
                                >
                                {{ __("dashboard/settings/exchangeRates.create.title") }}
                                </a
                            >
                        </li>
                    </ul>
                    <div class="tab-content">
                        
                        <div class="tab-pane body active" id="CreateExchangeRate">
                            <dashboard-administrative-settings-create-exchange-rates
                                :trans="{{$lang}}"
                            >
                            </dashboard-administrative-settings-create-exchange-rates>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active">
                            <a
                                class="nav-link"
                                data-toggle="tab"
                                href="#ShowExchangeRates"
                                >{{ __("dashboard/settings/exchangeRates.show.title") }}</a
                            >
                        </li>
                    </ul>
                    <div class="tab-content">
                        
                        <dashboard-administrative-settings-show-and-edit-exchange-rates
                            :lang="{{$lang}}"
                        ></dashboard-administrative-settings-show-and-edit-exchange-rates>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
@endsection