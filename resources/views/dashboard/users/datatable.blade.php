@extends('layouts.dashboard.app') 
@php 

    $lang = json_encode(__('dashboard/users/datatable.datatable')); 
@endphp
@section('content')
<section id="app" class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-5 col-sm-12">
                <h2>{{ __('dashboard/users/datatable.datatable_title') }}
                <small class="text-muted">{{__('dashboard/users/datatable.datatable_subtitle') }}</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-7 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="/"><i class="zmdi zmdi-home"></i> {{ __('dashboard/users/datatable.slug.home') }} </a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">{{ __('dashboard/users/datatable.slug.administrative') }}</a></li>
                    <li class="breadcrumb-item active">{{ __('dashboard/users/datatable.slug.user') }}</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-md-12">
                <div class="card patients-list">
                    <div class="header">
                        <h2>{!!__('dashboard/users/datatable.user_list') !!}</h2>
                        
                    </div>
                    <div class="body">
                        
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs padding-0">
                            @foreach ($users as $key => $value)
                                <li  class="nav-item">
                                    <a class="nav-link @if ($loop->first) active @endif" data-toggle="tab" href="#user{{$key}}">
                                        @if ($key == 100)
                                            {{__('dashboard/users/datatable.user_type.carer')}}
                                        @elseif($key == 200)
                                            {{ __('dashboard/users/datatable.user_type.user')}}
                                        @else
                                            {{ __('dashboard/users/datatable.user_type.admin')}}
                                        @endif
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                            
                        <!-- Tab panes -->
                        <div class="tab-content m-t-10">
                            @foreach ($users as $key => $user)
                                <dashboard-admininistrative-users-datatable  
                                    @if ($loop->first)
                                        :active="true"
                                    @else
                                        :active="false"
                                    @endif
                                    :user="{{$user}}"
                                    :clave="'{{$key}}'"
                                    :lang="{{$lang}}">
                                </dashboard-admininistrative-users-datatable>                                
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection