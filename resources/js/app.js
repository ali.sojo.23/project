/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import { CoolSelectPlugin } from 'vue-cool-select'
// paste the line below only if you need "bootstrap" theme
import 'vue-cool-select/dist/themes/bootstrap.css'
 require('./bootstrap');
window.Vue = require('vue');

 


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.use(CoolSelectPlugin)
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('dashboard-profile-profile-card', require('./components/dashboard/profile/ProfileCard.vue').default);
Vue.component('dashboard-profile-modal-update-img-profile', require('./components/dashboard/profile/ModelUpdateProfile.vue').default);
Vue.component('dashboard-profile-edit-account-reset-pass', require('./components/dashboard/profile/ResetPassword.vue').default);
Vue.component('dashboard-profile-edit-account-info', require('./components/dashboard/profile/EditInfoAccount.vue').default);
Vue.component('dashboard-profile-edit-verification-account-info', require('./components/dashboard/profile/VerificationUser.vue').default);
Vue.component('dashboard-administrative-settings-create-exchange-rates', require('./components/dashboard/settings/rates/CreateExchangeRates.vue').default);
Vue.component('dashboard-administrative-settings-show-and-edit-exchange-rates',require('./components/dashboard/settings/Rates/ShowAndEditExchangeRateDatatable.vue').default);
Vue.component('dashboard-admininistrative-users-datatable', require('./components/dashboard/users/UsersDatatable.vue').default);
Vue.component('dashboard-admininistrative-memberships-datatable', require('./components/dashboard/memberships/Datatable.vue').default);
Vue.component('dashboard-admininistrative-memberships-modal', require('./components/dashboard/memberships/Model.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
const app2 = new Vue({
    el: '#modal'
}) 
