<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Page Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "register_new_user" => "Registrer new user",
    "firstname" => "Firstname",
    "lastname" => "Lastname",
    "national_identification_number" => "National Identification Number",
    "country" => "Country"

];
