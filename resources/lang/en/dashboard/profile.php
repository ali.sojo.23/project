<?php

return [
    "profile_title" => "User Profile",
    "profile_title_cuidador" => "Carer Profile",
    "profile_title_admin" => "Administrator Profile",
    "profile_subtitle" => "Show the profile information",
    "modal" => [
        "edit_image_title" => "Edit the profile image",
        "edit_image_close" => "CLOSE",
        "edit_image_save" => "SAVE"
    ],
    "profile_card" => [
        "nurse" => "Carer",
        "admin" => "Administrator",
        "user"  => "User",
        "follow" => "Follow",
        "following" => "Following",
        "message" => "Message"
    ],
    "print_info" => [
        "about" => "About",
        "account" => "Edit account:",
        "general_info" => [
            "title" => "General information:",
            "gender" => "Gender:",
            "birthdate" => "Birthdate:"
        ]
    ],
    "reset_pass" => [
        "email" => "Email",
        "verify_email" => "Verify email",
        "oldpassword" => "Old password",
        "newpassword" => "New password",
        "button" => "Reset password",
        
    ],
    "edit_info" => [
        "firstname" => "First name",
        "lastname" => "Last name",
        "email" => "Email",
        "gender" => [
            "title" => "Gender",
            "male" => "Male",
            "female" => "Female",
            "no_binary" => "Genderqueer"
        ],
        "city" => [
            "title" => "City",
            "msg1" => "No information found by request.",
            "msg2" => "We need at least 2 letters to search."
        ],
        "country" => [
            "title" =>"Country",
            "msg1" => "No information found by request.",
            "msg2" => "We need at least 2 letters to search."
        ],
        "address_line_1" => "Address line 1",
        "address_line_2" => "Address line 2",
        "description" => "Description",
        "phone" => "Phone",
        "postal_code" => "Postal code"
    ],
    "response" => [
        'error' => [
            'newpassword_no_validate' => 'The password entered is not valid',
            'missmatch_pass' => 'The password entered is incorrect',
            'user_not_found' => 'The requested user is not registered in the database'
        ],
        'success' => [
            'password_reseted' => 'The password was changed successfully.',
            'profile_updated' => 'The user was updated successfully.'
        ]
    ]
];