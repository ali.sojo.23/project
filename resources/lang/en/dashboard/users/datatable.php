<?php

return [
    "datatable_title" => "User table",
    "datatable_subtitle" => "User data table",
    "slug" => [
        'home' => "home",
        'administrative' => 'Administration',
        "user" => "Users",
    ],
    "user_list" => "<strong>Users</strong> list",
    "user_type" => [
        "admin" => "Administrators",
        "carer" => "Carer",
        "user"  => "User"
    ],
    "datatable" => [
        "user_id" => "User ID",
        "firstname" => "Name",
        "lastname" => "lastname",
        "email" => "E-Mail",
        "verification" => "Verification",
        "action" => "Action",
        "verified" => "Verified",
        "non_verified" => "No Verified",
        "edit" => "Edit"
    ]

];