<?php

return [
    "loader" => [
        "wait" => "Please wait..."
    ],
    "rightbar" => [
        "setting_account" => "Account settings",
        "localization" => [
            "lang1" => 'Español',
            "lang1_flag" => 'flag flag-icon-es',
            "lang1_url" => "/setlang/es",
            "lang2" => 'Portugues',
            "lang2_flag" => 'flag flag-icon-br',
            "lang2_url" => "/setlang/pt_BR"
        ],
        "view_mode" => "Customize your view",
        "night_shift" => "Night mode",
        "light_shift" => "Light mode"
    ],
    "leftbar" => [
        "profile_card" => [
            "nurse" => "Carer",
            "admin" => "Admin",
            "user"  => "User",
            "follow" => "Follow",
            "following" => "Following",
            "message" => "Message",
            "email" => "Email",
            "location" => "Location",
            "phone" => "Phone",
            "website" => "Website"
        ],
        "menu" => [
            "administrative" => [
                "title" => "Administration",
                "user"  => "Users"
            ]
        ]
    ]
];
