<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Page Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "register_new_user" => "Sign in for user",
    "register_new_acompa" => "Sign in for carer",
    "firstname" => "Insert your name",
    "lastname" => "Insert your lastname",
    "terms_conditions" => "I have read and accept the <a target='_blank' href='https://xn--acompao-9za.com/terminos-y-condiciones'>terms and conditions</a>",
    "already_account" => "¿Do you have an account?"

];
