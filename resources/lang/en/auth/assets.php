<?php

return [
    "register_acompa" => "Sign in for carer",
    "register_user" => "Sign in for users",
    "contact_us" => 'CONTACT US',
    "contact_us_url" => 'https://xn--acompao-9za.com/make-appointment/',
    "about"=>"ABOUT US",
    "about_url"=>"https://xn--acompao-9za.com/about/",
    "blog" =>"BLOG",
    "blog_url" => "https://xn--acompao-9za.com/articles/",
    "lang1" => 'SPANISH',
    "lang1_url" => "/setlang/es",
    "lang2" => 'PORTUGUES',
    "lang2_url" => "/setlang/pt_BR"
];