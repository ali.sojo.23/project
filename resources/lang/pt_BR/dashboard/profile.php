﻿<?php

return [
    "profile_title" => "Perfil do usuário",
    "profile_title_cuidador" => "Perfil do cuidador",
    "profile_title_admin" => "Perfil do administrador",
    "profile_subtitle" => "Mostra informações de perfil",
    "modal" => [
        "edit_image_title" => "Editar imagem de perfil",
        "edit_image_close" => "FECHAR",
        "edit_image_save" => "SALVAR"
    ],
    "profile_card" => [
        "nurse" => "Cuidador",
        "admin" => "Administrador",
        "user"  => "Usuário",
        "follow" => "Seguir",
        "following" => "Seguiendo",
        "message" => "Mensagem"
    ],
    "print_info" => [
        "about" => "Sobre",
        "account" => "Editar Conta:",
        "general_info" => [
            "title" => "Informações gerais:",
            "gender" => "Gênero:",
            "birthdate" => "Data de nascimento:"
        ]
    ],
    "reset_pass" => [
        "email" => "E-mail",
        "verify_email" => "Verificar e-mail",
        "oldpassword" => "Senha anterior",
        "newpassword" => "Senha nova",
        "button" => "Redefinir senha",
        
    ],
    "edit_info" => [
        "firstname" => "Nome",
        "lastname" => "Apelido",
        "email" => "E-mail",
        "gender" => [
            "title" => "Gênero",
            "male" => "Masculino",
            "female" => "Fêmeo",
            "no_binary" => "Não binário"
        ],
        "city" => [
            "title" => "Cidade",
            "msg1" => "Nenhuma informação encontrada por solicitação.",
            "msg2" => "Precisamos de pelo menos 2 letras para pesquisar."
        ],
        "country" => [
            "title" =>"País",
            "msg1" => "Nenhuma informação encontrada por solicitação.",
            "msg2" => "Precisamos de pelo menos 2 letras para pesquisar."
        ],
        "address_line_1" => "Endereço linha 1",
        "address_line_2" => "Endereço linha 2",
        "description" => "Descrição",
        "phone" => "Telefone",
        "postal_code" => "Código postal"
    ],
    "response" => [
        'error' => [
            'newpassword_no_validate' => 'A senha digitada não é válida',
            'missmatch_pass' => 'A senha digitada está incorreta',
            'user_not_found' => 'O usuário solicitado não está registrado no banco de dados'
        ],
        'success' => [
            'password_reseted' => 'A senha foi alterada com sucesso.',
            'profile_updated' => 'O usuário foi atualizado de maneira satisfatória.'
        ]
    ]
];