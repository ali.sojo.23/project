<?php

return [
    "datatable_title" => "Tabela do usuário",
    "datatable_subtitle" => "Tabela de dados do usuário",
    "slug" => [
        'home' => "Início",
        'administrative' => 'Administração',
        "user" => "Usuário",
    ],
    "user_list" => "Lista de <strong>usuários</strong>",
    "user_type" => [
        "admin" => "Administradores",
        "carer" => "Cuidador",
        "user"  => "Usuário"
    ],
    "datatable" => [
        "user_id" => "ID do usuário",
        "firstname" => "Nome",
        "lastname" => "Último nome",
        "email" => "Email",
        "verification" => "verificação",
        "action" => "Açao",
        "verified" => "verificado",
        "non_verified" => "não verificado",
        "edit" => "Editar"
    ]

];