﻿<?php

return [
    "loader" => [
        "wait" => "Por favor, aguarde..."
    ],
    "rightbar" => [
        "setting_account" => "Configuração da conta",
        "localization" => [
            "lang1" => 'English',
            "lang1_flag" => 'flag flag-icon-us',
            "lang1_url" => "/setlang/en",
            "lang2" => 'Español',
            "lang2_flag" => 'flag flag-icon-es',
            "lang2_url" => "/setlang/es",
        ],
        "view_mode" => "Customize sua visualização",
        "night_shift" => "Modo noturno",
        "light_shift" => "Modo claro"
    ],
    "leftbar" => [
        "profile_card" => [
            "nurse" => "Cuidador",
            "admin" => "Admin",
            "user"  => "Usuário",
            "follow" => "Siga",
            "following" => "Seguindo",
            "message" => "Mensagem",
            "email" => "E-mail",
            "location" => "Localização",
            "phone" => "Telefone",
            "website" => "Website",
            
        ],
        "menu" => [
            "administrative" => [
                "title" => "Administração",
                "user"  => "Usuários"
            ]
        ]
    ]
];