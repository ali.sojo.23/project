<?php

return [
    "register_acompa" => "Cadastro de cuidador",
    "register_user" => "Cadastro de usu�rios",
    "contact_us" => 'FALE CONOSCO',
    "contact_us_url" => 'https://xn--acompao-9za.com/make-appointment/',
    "about"=>"SOBRE NOS",
    "about_url"=>"https://xn--acompao-9za.com/about/",
    "blog" =>"BLOG",
    "blog_url" => "https://xn--acompao-9za.com/articles/",
    "lang1" => 'ENGLISH',
    "lang1_url" => "/setlang/en",
    "lang2" => 'ESPAÑOL',
    "lang2_url" => "/setlang/es"
];