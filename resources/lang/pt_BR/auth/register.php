﻿<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Register Page Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    "register_new_user" => "Cadastro de novo usuário",
    "register_new_acompa" => "Cadastro de novo cuidador",
    "firstname" => "Insira seu nome",
    "lastname" => "Insira seu apelido",
    "terms_conditions" => "Eu li e aceito os <a target='_blank' href='https://xn--acompao-9za.com/terminos-y-condiciones'>termos e condições</a>",
    "already_account" => "Já é membro?"

];
