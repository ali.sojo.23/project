<?php

return [
    "register_acompa" => "Registro de cuidador",
    "register_user" => "Registro de usuario",
    "contact_us" => 'CONTACTANOS',
    "contact_us_url" => 'https://xn--acompao-9za.com/make-appointment/',
    "about"=>"NOSOTROS",
    "about_url"=>"https://xn--acompao-9za.com/about/",
    "blog" =>"BLOG",
    "blog_url" => "https://xn--acompao-9za.com/articles/",
    "lang1" => 'ENGLISH',
    "lang1_url" => "/setlang/en",
    "lang2" => 'PORTUGUES',
    "lang2_url" => "/setlang/pt_BR"
];