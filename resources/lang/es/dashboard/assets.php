<?php

return [
    "loader" => [
        "wait" => "Por favor espere..."
    ],
    "rightbar" => [
        "setting_account" => "Configuracion de la cuenta",
        "localization" => [
            "lang1" => 'English',
            "lang1_flag" => 'flag flag-icon-us',
            "lang1_url" => "/setlang/en",
            "lang2" => 'Portugues',
            "lang2_flag" => 'flag flag-icon-br',
            "lang2_url" => "/setlang/pt_BR"
        ],
        "view_mode" => "Personaliza tu visualización",
        "night_shift" => "Modo nocturno",
        "light_shift" => "Modo claro"
    ],
    "leftbar" => [
        "profile_card" => [
            "nurse" => "Cuidador",
            "admin" => "Admin",
            "user"  => "Usuario",
            "follow" => "Seguir",
            "following" => "Siguiendo",
            "message" => "Mensaje",
            "email" => "Correo electrónico",
            "location" => "Ubicación",
            "phone" => "Telefono",
            "website" => "Website"
        ],
        "menu" => [
            "administrative" => [
                "title" => "Administracion",
                "user"  => "Usuarios",
                "config" => [
                    "title" => "Configuración",
                    "exchange_rate" => "Tasa de cambio"
                ],
                "membership" => "Membresias"
            ]
        ]
    ]
];
