<?php

return [
    "profile_title" => "Perfil de usuario",
    "profile_title_cuidador" => "Perfil del cuidador",
    "profile_title_admin" => "Perfil del administrador",
    "profile_subtitle" => "Muestra la información de perfil",
    "modal" => [
        "edit_image_title" => "Editar imagen de perfil",
        "edit_image_close" => "CERRAR",
        "edit_image_save" => "GUARDAR"
    ],
    "profile_card" => [
        "nurse" => "Cuidador",
        "admin" => "Administrador",
        "user"  => "Usuario",
        "follow" => "Seguir",
        "following" => "Siguiendo",
        "message" => "Mensaje"
    ],
    "print_info" => [
        "about" => "Sobre",
        "account" => "Editar Cuenta:",
        "verify" => "Verificar Cuenta:",
        "general_info" => [
            "title" => "Información general:",
            "gender" => "Genero:",
            "birthdate" => "Fecha de nacimiento:"
        ]
    ],
    "reset_pass" => [
        "email" => "Correo electrónico",
        "verify_email" => "Verificar correo",
        "oldpassword" => "Contraseña anterior",
        "newpassword" => "Contraseña nueva",
        "button" => "Resetear contraseña",
        
    ],
    "edit_info" => [
        "firstname" => "Nombre",
        "lastname" => "Apellido",
        "email" => "Correo electrónico",
        "gender" => [
            "title" => "Género",
            "male" => "Masculino",
            "female" => "Femenino",
            "no_binary" => "No binário"
        ],
        "city" => [
            "title" => "Ciudad",
            "msg1" => "No se encontró información solicitada.",
            "msg2" => "Necesitamos al menos 2 letras para buscar."
        ],
        "country" => [
            "title" =>"País",
            "msg1" => "No se encontró información solicitada.",
            "msg2" => "Necesitamos al menos 2 letras para buscar."
        ],
        "address_line_1" => "Dirección linea 1",
        "address_line_2" => "Dirección linea 2",
        "description" => "Descripción",
        "phone" => "Telefono",
        "postal_code" => "Código postal"
    ],
    "verification_user" => [
        "msg1" => "Su usuario no ha sido verificado, para poder continuar con la públicación de su perfil debe completar con los siguiente requisitos",
        "l1"   => "Foto de perfil con la cedula",
        "l2"   => "Foto de la cedula sin edición",
        "next" => "Continuar",
        "msg2" => "Aún no verifica la cuenta de correo eléctrónico. Presione en el botón de reenviar correo de confirmación para confirmar la cuenta de correo electrónico",
        "resend_email" => "Reenviar correo de confirmación",
        "profile_pic" => "Foto de Perfil",
        "change_pic" => "Cambiar foto",
        "card_img" => "Foto de cédula",
        "load_info" => "Cargar información",
        "msg3" => "Su usuario se encuentra en proceso de verificación",
        "download_img" => "Descargar imagen",
        "verify_user" => "Verificar usuario",
        "msg4" => "La cuenta de usuario fue verificada exitosamente."

    ],
    "response" => [
        'error' => [
            'newpassword_no_validate' => 'La contraseña ingresada no es valida',
            'missmatch_pass' => 'La contraseña introducida es incorrecta',
            'user_not_found' => 'El usuario solicitado no se encuentra regisitrado en la base de datos'
        ],
        'success' => [
            'password_reseted' => 'La contraseña fué cambiada con exito.',
            'profile_updated' => 'El usuario fue actualizado de manera satisfactoria.'
        ]
    ]
];