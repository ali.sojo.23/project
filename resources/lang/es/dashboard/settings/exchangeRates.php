<?php

return [
    "title" => "Tasa de cambio",
    "slug" => [
        "home" => "Home",
        "administrative" => "Administración",
        "settings" => "Configuraciones",
        "exchange_rates" => "Tasa de Cambios"
    ],
    "create" => [
        "title" => "Agregar tasa de cambio",
        "country" => [
            "title" =>"País",
            "msg1" => "No se encontró información solicitada.",
            "msg2" => "Necesitamos al menos 2 letras para buscar."
        ],
        "exchange_rate" => "Tasa de cambio",
        "courrency" => "Mondeda ejemplo: USD"
    ],
    "show" => [
        "title" => "Tasas de cambios en el sistema",
        "country" => "Pais",
        "rate" => "T. de cambio",
        "action" => "Acciones",
        "buttons" => [
            "edit" => "<i class='zmdi zmdi-edit'></i>",
            "delete" => "<i class='zmdi zmdi-delete'></i>",
            "accept" => "<i class='zmdi zmdi-check'></i>",
            "cancel" => "<i class='zmdi zmdi-close'></i>"
        ]
    ],
    "response" => [
        "error" => [
            "country_exist" => "El país seleccionado ya posee una Tasa de Cambio creada, recomendamos ir a la seccion de edición y hacer los cambios necesarios."
        ],
        "success" => [
            "create" => "La tasa de cambios fue creada exitosamente. La página se actualizará automaticamente en 3 segundos",
            "update" => "La tasa de cambios fue actualizada exitosamente."
        ]
    ]
];