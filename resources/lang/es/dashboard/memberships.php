<?php

return [
    "title" => "Tabla de Membresias",
    "subtitle" => "<strong>Membresías</strong> por pais.",
    "slug" => [
        "home" => "Home",
        "administrative" => "Administración",
        "memberships" => "Membresias"
    ],
    "create" => [
        "title" => "Crear una nueva membresía",
        "msg1" => "No se encontró información solicitada.",
        "msg2" => "Necesitamos al menos 2 letras para buscar.",
        "buttons" => [
            "change_country" => "Cambiar País",
            "change_image" => "Cambiar Imagen",
            "close" => "Cerrar",
            "save" => "Guardar"
        ],
        "placeholders" => [
            "title" => "Nombre de la membresía",
            "price" => "Precio mensual de la membresía",
            "description" => "Descripción de la membresía",
            "feature_image" => "Imágen Caráteristica"
        ]
    ],
    "datatable" => [
        "membership" => "Membresias",
        "price" => "Precio",
        "action" => "Accioes",
        "edit" => "Editar",
        "delete" => "Eliminar",
        "empty" => "No existen datos para mostrar en la tabla",
        "edition" => [
            "title" => "Titulo",
            "price" => "Precio",
            "description" => "Descripción",
            "save" => "Guardar",
            "cancel" => "Cancelar",
            "edit_image" => "Editar imágen"
        ]

    ]
];