<?php

return [
    "datatable_title" => "Tabla de usuarios",
    "datatable_subtitle" => "Tabla de datos de usuarios",
    "slug" => [
        'home' => "Home",
        'administrative' => 'Administración',
        "user" => "Usuarios",
    ],
    "user_list" => "Lista de <strong>usuarios</strong>",
    "user_type" => [
        "admin" => "Administradores",
        "carer" => "Cuidadores",
        "user"  => "Usuarios"
    ],
    "datatable" => [
        "user_id" => "ID",
        "firstname" => "Nombre",
        "lastname" => "Apellido",
        "email" => "E-Mail",
        "verification" => "Verificación",
        "action" => "Acción",
        "verified" => "Verificado",
        "non_verified" => "No verificado",
        "edit" => "Editar"
    ]

];