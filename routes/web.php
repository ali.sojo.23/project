<?php

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('profile');
});
Route::get('setlang/{lang}',function($lang){
    Cookie::queue( Cookie::make('lang', $lang, 1440) );
    return redirect()->back();
});
Route::get('setmode/{mode}',function($mode){
    Cookie::queue( Cookie::make('mode', $mode, 1440) );
    return redirect()->back();
});

Auth::routes(['verify' => true]);
Route::group(['namespace' => 'App\Http\Controllers\views'],function(){
    // rutas de autorizacion
    Route::group(['namespace' => 'auth'],function(){
        Route::get('register-avanced-user','AuthController@registerAcomp')->name('register_acompa');
    });
    // fin de autorizacion
    // tabla de datos
    Route::group(['namespace' => 'dashboard', 'middleware' => 'auth'],function(){
        // Todos los usuarios
        Route::get('profile','ProfileController@profile')->name('profile');
        // fin todos los usuarios
        // Usuarios administrativos
        Route::group(['prefix' => 'administrative', 'middleware' => 'verify.admin.role'], function (){
            Route::get('users','UsersController@index')->name('admin.users.datatable');
            Route::get('users/{user}','UsersController@show')->name('admin.users.show');
            Route::group(['prefix' => 'settings'],function(){
                Route::get('rates','ConfigController@rates')->name('admin.settings.rate');
            });
            Route::get('memberships','MembershipsController@index')->name('admin.memberships.index');
        });
        // fin de usuarios administrativos
        
    });
    // fin tabla de datos
});
