<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'namespace' => 'App\Http\Controllers\backend',
], function(){
    Route::group(['prefix' => 'users'], function(){
        Route::apiResource('information','InfoUserController')->except(['create','edit']);
        Route::apiResource('edit','UsersController')->except(['create','edit']);
        Route::apiResource('verification','VerificationUsersController')->except(['create','edit']);
    });
    Route::group(['prefix' => 'data'], function(){
        Route::get('countries/{country}','CitizienController@country')->name('data.country');
        Route::get('states/{country}/{state}','CitizienController@state')->name('data.country.state');
    });
    Route::group(['prefix' => 'settings'], function(){
        Route::apiResource('rates','TasaDeCambioController')->except(['create','edit']);
    });
    Route::apiResource('memberships','MembershipsController')->except(['create','edit']);
});
