<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tasa_de_cambio extends Model
{
    use HasFactory;
    protected $fillable = [
        'countries_id',
        'rate',
        'courrency'
    ];
    protected function pais(){
        return $this->hasOne('App\Models\countries','id','countries_id');
    }
    protected function memberships(){
        return $this->hasMany('App\Models\memberships','rate_id','id');
    }
}
