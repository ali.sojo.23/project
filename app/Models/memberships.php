<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class memberships extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'rate_id',
        'price',
        'title',
        'description',
        'feature_image',
    ];
    protected $hidden = [
        'rate_id',
    ];
    protected function rate(){
        return $this->hasOne('App\Models\tasa_de_cambio','id','rate_id');
    }

}
