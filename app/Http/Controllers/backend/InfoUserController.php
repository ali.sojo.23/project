<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\info_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class InfoUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {;
        $info = info_user::where('user_id', $request['user_id'])->first();
        if ($request['img_profile']) {
            $image = $request['img_profile'];;  // your base64 encoded
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = Str::random(10) . date('U') . '.' . 'jpeg';
            $path = 'users/profile/' . $imageName;
            Storage::disk('public')->put($path, base64_decode($image));
            $path = env('APP_URL') . "/storage/" . $path;
            if ($info) {
                $info->img_profile = $path;
                $info->save();
            } else {
                $info = info_user::create([
                    'user_id' => $request['user_id'],
                    'img_profile' => $path
                ])->save();
            }
            return response(['message' => __('dashboard/profile.response.success.profile_updated'), 'info' => $info], 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\info_user  $info_user
     * @return \Illuminate\Http\Response
     */
    public function show(info_user $info_user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\info_user  $info_user
     * @return \Illuminate\Http\Response
     */
    public function edit(info_user $info_user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\info_user  $info_user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $i = info_user::where('user_id', $request['id'])->first();
        $info = $request['info'];

        if (!$i) {
            $i = info_user::create([
                'user_id' => $request['id'],
                'country' => $info['country'],
                'ciudad' => $info['ciudad'],
                'address_1' => $info['address_1'],
                'address_2' => $info['address_2'],
                'phone' => $info['phone'],
                'description' => $info['description'],
                'dni' => $info['dni'],
                'postal_code' => $info['postal_code'],
                'gender' => $info['gender'],
                'birthdate' => $info['birthdate']
            ]);
        } else {
            $i->country = $info['country'];
            $i->ciudad = $info['ciudad'];
            $i->address_1 = $info['address_1'];
            $i->address_2 = $info['address_2'];
            $i->phone = $info['phone'];
            $i->description = $info['description'];
            $i->dni = $info['dni'];
            $i->postal_code = $info['postal_code'];
            $i->gender = $info['gender'];
            $i->birthdate = $info['birthdate'];
        }

        $i->save();
        return response(['message' => __('dashboard/profile.response.success.profile_updated'), 'info' => $i], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\info_user  $info_user
     * @return \Illuminate\Http\Response
     */
    public function destroy(info_user $info_user)
    {
        //
    }
}
