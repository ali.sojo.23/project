<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\memberships;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class MembershipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $a = memberships::create([
            'rate_id' => $request['rate_id'],
            'price' => $request['price'],
            'title' => $request['title'],
            'description' => $request['description'],
            'feature_image' => $request['feature_image'],
        ]);
        return $a;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\memberships  $memberships
     * @return \Illuminate\Http\Response
     */
    public function show(memberships $memberships)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\memberships  $memberships
     * @return \Illuminate\Http\Response
     */
    public function edit(memberships $memberships)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\memberships  $memberships
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, memberships $membership)
    {
        if($membership){
            $membership->price = $request['price'];
            $membership->title = $request['title'];
            $membership->description = $request['description'];
            $membership->feature_image = $request['feature_image'];
            $membership->save();
            return $membership;
        }else{
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\memberships  $memberships
     * @return \Illuminate\Http\Response
     */
    public function destroy(memberships $membership)
    {
        if($membership){
            $membership->delete();
            return $membership;
        }else{
            abort(403);
        }
    }
}
