<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if($user){
            // Reset Password 
            if($request['newpassword'] && $request['oldpassword']){
                   $validator = Validator::make($request->toArray(),[
                        'newpassword' => ['required','string','min:8'],
                    ]);
                    if( ! $validator->fails()){
                        
                        $pass = Hash::check($request['oldpassword'],$user->password);
                        if($pass){
                            $user->password = Hash::make($request['newpassword']);
                            $user->save();
                            return response(['message' => __('dashboard/profile.response.success.password_reseted')],200);
                        }else{
                            return response(['message' => __('dashboard/profile.response.error.missmatch_pass')],409);
                        }
                    }else{
                        return response(['message' => __('dashboard/profile.response.error.newpassword_no_validate')],409);
                    }
            }
            // final del reset password 
        }else{
            return response(['message' => __('dashboard/profile.response.error.user_not_found')],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
