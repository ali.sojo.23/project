<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\countries;
use App\Models\states;
use Illuminate\Http\Request;


class CitizienController extends Controller
{
    public function country($country){
        $c = countries::where('name','like','%'.$country.'%')->get();
        return $c;
    }
    public function state($country,$state){
        $c = states::where('id_country',$country)->where('name','like','%'.$state.'%')->get();
        return $c;
    }
}
