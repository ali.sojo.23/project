<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;;
use App\Models\tasa_de_cambio;
use Illuminate\Http\Request;

class TasaDeCambioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $t = tasa_de_cambio::all();
        foreach ($t as $key => $i) {
            $i->pais;
        }
        return $t;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $t = tasa_de_cambio::where('countries_id',$request['country'])->first();
        if(!$t){
            tasa_de_cambio::create([
                'countries_id' => $request['country'],
                'rate' => $request['rate'],
                'courrency' => $request['courrency'],
            ]);
            return response(['message' => __("dashboard/settings/exchangeRates.response.success.create")],200);
        }else{
            return response(['message' => __("dashboard/settings/exchangeRates.response.error.country_exist")],409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\tasa_de_cambio  $tasa_de_cambio
     * @return \Illuminate\Http\Response
     */
    public function show(tasa_de_cambio $tasa_de_cambio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\tasa_de_cambio  $tasa_de_cambio
     * @return \Illuminate\Http\Response
     */
    public function edit(tasa_de_cambio $tasa_de_cambio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\tasa_de_cambio  $tasa_de_cambio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tasa_de_cambio $rate)
    {
        $rate->courrency = $request['courrency'];
        $rate->rate = $request['rate'];
        $rate->save();

        return response(['message' => __("dashboard/settings/exchangeRates.response.success.update")],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\tasa_de_cambio  $tasa_de_cambio
     * @return \Illuminate\Http\Response
     */
    public function destroy(tasa_de_cambio $tasa_de_cambio)
    {
        //
    }
}
