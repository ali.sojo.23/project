<?php

namespace App\Http\Controllers\views\dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class ProfileController extends Controller
{
    public function profile()
    {


        if (Cookie::get('mode') == 'dark') {
            $css = "assets/css/dark.css";
        } else {
            $css = "assets/css/main.css";
        };
        $user = Auth::user();
        $user->info;
        if($user->info){
            $user->info->pais;
            $user->info->estado;
        }
        
        return view('dashboard.profile.show', [
            "styles" => [
                "assets/plugins/bootstrap/css/bootstrap.min.css",
                $css,
                "css/flag-icon.min.css",
                "assets/css/color_skins.css"
            ],
            "user" => Auth::user(),
            "scripts" => [
                "assets/bundles/libscripts.bundle.js",
                "assets/bundles/vendorscripts.bundle.js",
                "assets/bundles/mainscripts.bundle.js",
            ],

        ]);
    }
}
