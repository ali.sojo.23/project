<?php

namespace App\Http\Controllers\views\dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class ConfigController extends Controller
{
    public function rates(){
        if (Cookie::get('mode') == 'dark') {
            $css = "assets/css/dark.css";
        } else {
            $css = "assets/css/main.css";
        };
        
        return view('dashboard.settings.exchangeRates', [
            "styles" => [
                "assets/plugins/bootstrap/css/bootstrap.min.css",
                $css,
                "css/flag-icon.min.css",
                "assets/css/color_skins.css"
            ],
            "scripts" => [
                "assets/bundles/libscripts.bundle.js",
                "assets/bundles/vendorscripts.bundle.js",
                "assets/bundles/mainscripts.bundle.js",
            ],

        ]);
    }
}
