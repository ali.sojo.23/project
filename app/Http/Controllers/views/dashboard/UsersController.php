<?php

namespace App\Http\Controllers\views\dashboard;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Cookie;

class UsersController extends Controller
{
    public function index(){
        if (Cookie::get('mode') == 'dark') {
            $css = "assets/css/dark.css";
        } else {
            $css = "assets/css/main.css";
        };
        $user = User::all()->groupBy('role');
        
        return view('dashboard.users.datatable', [
            "styles" => [
                "assets/plugins/bootstrap/css/bootstrap.min.css",
                $css,
                "css/flag-icon.min.css",
                "assets/css/color_skins.css"
            ],
            "users" => $user,
            "scripts" => [
                "assets/bundles/libscripts.bundle.js",
                "assets/bundles/vendorscripts.bundle.js",
                "assets/bundles/mainscripts.bundle.js",
            ],

        ]);
    }
    public function show($user){
        if (Cookie::get('mode') == 'dark') {
            $css = "assets/css/dark.css";
        } else {
            $css = "assets/css/main.css";
        };
        $user = User::where('id',$user)->first();
        $user->info;
        if($user->info){
            $user->info->pais;
            $user->info->estado;
        }
        
        return view('dashboard.profile.show', [
            "styles" => [
                "assets/plugins/bootstrap/css/bootstrap.min.css",
                $css,
                "css/flag-icon.min.css",
                "assets/css/color_skins.css"
            ],
            "user" => $user,
            "scripts" => [
                "assets/bundles/libscripts.bundle.js",
                "assets/bundles/vendorscripts.bundle.js",
                "assets/bundles/mainscripts.bundle.js",
            ],

        ]);
    }
}
