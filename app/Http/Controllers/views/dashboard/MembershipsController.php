<?php

namespace App\Http\Controllers\views\dashboard;

use App\Http\Controllers\Controller;
use App\Models\memberships;
use App\Models\tasa_de_cambio;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class MembershipsController extends Controller
{
    public function index(){
        if (Cookie::get('mode') == 'dark') {
            $css = "assets/css/dark.css";
        } else {
            $css = "assets/css/main.css";
        };
        $m = tasa_de_cambio::all();
        // $m = memberships::all();
        foreach ($m as $me) {
            $me->pais;
            $me->memberships;
        }
        // $m = $m->groupBy('rate_id');
        // return $m;
        return view('dashboard.memberships.datatable', [
            "styles" => [
                "assets/plugins/bootstrap/css/bootstrap.min.css",
                $css,
                "css/flag-icon.min.css",
                "assets/css/color_skins.css"
            ],
            "m" => $m,
            "scripts" => [
                "assets/bundles/libscripts.bundle.js",
                "assets/bundles/vendorscripts.bundle.js",
                "assets/bundles/mainscripts.bundle.js",
            ],

        ]);
    }
}
