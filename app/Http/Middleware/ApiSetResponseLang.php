<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\App; 
use Illuminate\Support\Facades\Cookie;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class ApiSetResponseLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        
        
        $res = Cookie::get('lang');
        
        App::setLocale( $res ?: config('app.locale') );
        
        return $next($request);
    }
}
