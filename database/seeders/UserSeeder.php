<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'Ali',
            'lastname' => 'Sojo',
            'email' => 'asojo@incgoin.com',
            'password' => Hash::make('password'),
            'role' => '1'
        ]);
        DB::table('users')->insert([
            'firstname' => 'fabricio',
            'lastname' => 'Rossi',
            'email' => 'frossi@incgoin.com',
            'password' => Hash::make('password'),
            'role' => '1'
        ]);

        if(App::environment('local')){
            \App\Models\User::factory(10)->create();
        }
    }
}
